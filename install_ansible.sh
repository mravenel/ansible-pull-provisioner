#!/usr/bin/env bash

# Install ansible on Ubuntu
apt --yes update
apt --yes install software-properties-common
add-apt-repository --yes --update ppa:ansible/ansible
apt --yes install ansible